# Datavard BWFT analysis
# Version 0.4
# last update: 13/06/2017
# Author: Peter Gabas (peter.gabas@datavard.com)

# import basic python libraries
import numpy as np
import pandas as pd
import openpyxl, pprint, warnings
import glob
from pathlib import Path

# import visualisation libraries
import matplotlib.pyplot as plt
import plotly
import plotly.offline as py
import plotly.graph_objs as go
import plotly.tools as tls
import seaborn as sns
plotly.offline.init_notebook_mode()

# suppress warnings
warnings.simplefilter("ignore")

# All files should be in /Data/ folder under BWFT
folder_name = 'BWFT'

# import HeatMap query statistics from Excel to Pandas dataframe (DF) structure and delete obsolete columns
BWFT_main = Path("../Data/" + folder_name + "/BWFT_Customers.xlsx").resolve()
print('\n->Importing BWFT main from file: '+ str(BWFT_main) + '...')
bwft_df = pd.read_excel(BWFT_main, index_col = 0, encoding='utf-8')
print('Done importing BWFT main from file. New dataframe created: bwft_df')

# let's have a look at the data
print(bwft_df.head())

# all BWFT data are separated to several analysis groups - called collectors. We'll load the exported data from HIVE table
print('\n->Loading files from all BWFT collectors, creating new dataframe - collectors')
path = Path("../Data/" + folder_name).resolve()
allFiles = glob.glob(str(path) + "/IBW*.xlsx")
collectors = pd.DataFrame()

# create list of all excel files & merge all excel files into one dataframe (df)
files = []
for file_ in allFiles:
    df = pd.read_excel(file_, encoding='utf-8', index_col = 0)
    files.append(df)

collectors = pd.concat(files)
print('Done loading collectors files')

# check the df
print(collectors.head())

# let's delete obsolete runids
bwft_af = collectors
print('\n->Dropping irrelevant RUNIDS, this may take a while...')
for i, row in collectors.iterrows():
        if (i not in bwft_df.index):
            bwft_af.drop([i], inplace = True)
print('Done dropping irrelevant RUNIDS')

#check the df again
print(bwft_af.head())

# ok, let's make a new df without the ANALYSIS column and we'll convert the rows into columns
bwft_af = bwft_af.drop('ANALYSIS', axis = 1)
bwft = bwft_af.reset_index().pivot(index = 'RUNID', columns = 'FIELDNAME', values = 'VALUE')
print(bwft.head())

# we need to correct some missing data before moving on
bwft.dropna(axis=1, how='all', inplace = True)
bwft.fillna(value = 0, inplace = True)
# merge together customer info and our dataset
df = pd.concat([bwft, bwft_df], axis=1, join='inner')
print(df.head())

df = df.replace(0, np.NaN)

#get info about size of different objects
DSO_data_perc = df["DSO"].mean()
print("DSO data average (of system size): " + str('%g' % DSO_data_perc) + "%")

WDSO_data_perc = df["WDSO"].mean()
print("WDSO data average (of system size): " + str('%g' % WDSO_data_perc) + "%")

CUBE_F_data_perc = df["Cube F"].mean()
print("Cube F data average (of system size): " + str('%g' % CUBE_F_data_perc) + "%")

CUBE_E_data_perc = df["Cube E"].mean()
print("Cube E data average (of system size): " + str('%g' % CUBE_E_data_perc) + "%")

CUBE_D_data_perc = df["Cube D"].mean()
print("Cube D data average (of system size): " + str('%g' % CUBE_D_data_perc) + "%")

PSA_data_perc = df["PSA"].mean()
print("PSA data average (of system size): " + str('%g' % PSA_data_perc) + "%")

MD_data_perc = df["Master Data"].mean()
print("Master data average (of system size): " + str('%g' % MD_data_perc) + "%")

HK_data_perc = df["Housekeeping"].mean()
print("Housekeeping data average (of system size): " + str('%g' % HK_data_perc) + "%")

Errorstack_data_perc = df["Errorstack"].mean()
print("Errorstack data average (of system size): " + str('%g' % Errorstack_data_perc) + "%")

Aggregate_data_perc = df["Aggregate"].mean()
print("Aggregate data average (of system size): " + str('%g' % Aggregate_data_perc) + "%")

Changelog_data_perc = df["Changelog"].mean()
print("Changelog data average (of system size): " + str('%g' % Changelog_data_perc) + "%")

Other_data_perc = df["Other data"].mean()
print("Other data average (of system size): " + str('%g' % Other_data_perc) + "%\n" )

# get info about hot/warm/cold data

#df_hana = (df[df["DB_VENDOR"] == 'HDB'])
new_data_perc = df["PERCENTG"].mean()
print("Hot data requests (<1 year): " + str('%g' % new_data_perc) + "%")


mid_data_perc = df["PERCENTM"].mean()
print("Warm data requests (1-2 years): " + str('%g' % mid_data_perc) + "%")

old_data_perc = df["PERCENTO"].mean()
print("Cold data requests (>2 years): " + str('%g' % old_data_perc) + "%")

    MD_data_unused_perc = df["MD_UNUSED_SIZE_PERC"].mean()
print("Master data unused average (of system size): " + str('%g' % MD_data_unused_perc) + "%")

# make a graph of data size distribution by data type
trace1 = go.Bar(
    x=['DSO', 'WDSO', 'Cube F', 'Cube E', 'Cube D', 'PSA', 'Master Data', 'Housekeeping', 'Errorstack', 'Aggregate', 'Changelog', 'Other data'],
    y=[DSO_data_perc, WDSO_data_perc, CUBE_F_data_perc, CUBE_E_data_perc, CUBE_D_data_perc, PSA_data_perc, MD_data_perc, HK_data_perc, Errorstack_data_perc, Aggregate_data_perc, Changelog_data_perc, Other_data_perc],
    marker=dict(color=['#E45751', '#FFD99E', '#3D94E5', '#FF8484', '#4DCCBD', '#FFF9C6', '#C7EFCF', '#F7DBA7', '#F0B67F', '#B4DC7F', '#DAB6C4', '#F4E285', '#EC4067', '#5BC8AF', '#E71D36', '#C9FFA0']),
    opacity=0.8,
)

data = [trace1]
layout = go.Layout(
    title='Average data distribution by type (in % of system size)',
)

fig = go.Figure(data=data, layout=layout)
py.iplot(fig, filename='Average data request distribution by type')


# make a graph of hot/warm/cold distribution
trace3 = go.Bar(
    x=['Hot data (<1 year)', 'Warm data (1-2 years)', 'Cold data (>2 years)'],
    y=[new_data_perc, mid_data_perc, old_data_perc],
    marker=dict(color=['#E45751', '#F4E285', '#ABD9F4']),
    opacity=0.8,
)

data = [trace3]
layout = go.Layout(
    title='Average data request distribution by age (in % of data size)',
)

fig = go.Figure(data=data, layout=layout)
py.iplot(fig, filename='Average data request distribution by age')

df["TOTAL_SIZE_GB"] = 0.0
for i, row in df.iterrows():
    total = row["FREESPACE_GB"] + row["SIZEUSED_GB"]
    df.loc[i, "TOTAL_SIZE_GB"] = total
total_size_all = df["TOTAL_SIZE_GB"].sum()
average_size_all = df["TOTAL_SIZE_GB"].mean()
print("Total size of all analyzed systems: " + str(total_size_all/1024/1024) + " PB")


# make a graph of total size of all systems
trace1 = go.Bar(
    x=[total_size_all/1024, average_size_all/1024],
    y=['Total size', 'Average size'],
    marker=dict(color=['#ABD9F4', '#E45751']),
    #width = [0.3, 0.3],
    opacity=0.8,
    orientation = 'h',
)

data = [trace1]
layout = go.Layout(
    title='Total size of all analyzed systems and average size in TB',
)

fig = go.Figure(data=data, layout=layout)
py.iplot(fig, filename='Total size of all analyzed systems')

# Average size used vs free
Size_used = df["SIZEUSED_PERC"]
Size_free = df["FREESPACE_PERC"]
#print(type(Size_used.mean()))

trace3 = go.Bar(
    x=['Size used', 'Size free'],
    y=[Size_used.mean(), Size_free.mean()],
    marker=dict(color=['#E45751', '#ABD9F4']),
    opacity=0.8,
)

data = [trace3]
layout = go.Layout(
    title='Average % of used / free space',
)

fig = go.Figure(data=data, layout=layout)
py.iplot(fig, filename='Average system size: used vs. free space')

# get some simple subsets of our dataframe
# DB vendors
df_hana = (df[df["DB_VENDOR"] == 'HDB'])
df_oracle = (df[df["DB_VENDOR"] == 'ORACLE'])
df_IBM = df[(df["DB_VENDOR"] == "DB2") | (df["DB_VENDOR"] == "DB6")]

# unique customers
df_customers = df.drop_duplicates(["Customer"])
country_hist = df_customers["Country"].value_counts()
industry_hist = df_customers["Industry"].value_counts()
#print(country_hist)

#customers by country - choropleth map
data = [ dict(
        type = 'choropleth',
        locationmode = 'country names',
        locations = country_hist.index,
        z = country_hist.values,
        text = country_hist.index,
        colorscale = [[0,"rgb(5, 10, 172)"],[0.25,"rgb(40, 60, 190)"],[0.5,"rgb(70, 100, 245)"],
                      [0.7,"rgb(90, 120, 245)"],[0.9,"rgb(106, 137, 247)"],[1,"#A8B9F7"]],
        autocolorscale = False,
        reversescale = True,
        colorbar = dict(
            autotick = False,
            title = 'Number of customers<br>'),
      ) ]

layout = dict(
    title = 'Number of customers by country',
    geo = dict(
        showcoastlines = True,
        showcountries = True,
        showframe = False,
        projection = dict(
            type = 'mercator'
        )
    ),
    width= 1200,
    margin = dict(
        l=0,
        r=0,
        b=50,
        t=50)
)

fig = dict( data=data, layout=layout )
py.iplot( fig, validate=False, filename='d3-world-map' )

# Customers by country - simple pie
labels1 = country_hist.index
values1 = country_hist.values
colors1 = ['#3D94E5', '#FF8484', '#4DCCBD', '#FFF9C6', '#C7EFCF', '#F7DBA7', '#F0B67F', '#B4DC7F', '#DAB6C4', '#F4E285', '#EC4067', '#5BC8AF', '#E71D36', '#C9FFA0']
trace1 = go.Pie(name="X", labels=labels1, values=values1, marker=dict(colors=colors1))
py.iplot([trace1], filename="Customers by country")

# Customers by industry - simple pie
labels2 = industry_hist.index
values2 = industry_hist.values
trace2 = go.Pie(labels=labels2, values=values2, marker=dict(colors=colors1))
py.iplot([trace2], filename="Customers by industry")

# system versions
bw_versions = df["BW_VER"].value_counts()
#bw_versions.index = bw_versions.index.map(str)

trace = go.Bar(
    x=bw_versions.index,
    y=bw_versions.values,
    marker=dict(color=['#E45751', '#FFD99E', '#3D94E5', '#FF8484', '#4DCCBD', '#FFF9C6', '#C7EFCF', '#F7DBA7', '#F0B67F', '#B4DC7F', '#DAB6C4', '#F4E285', '#EC4067', '#5BC8AF', '#E71D36', '#C9FFA0']),
    opacity=0.8,
)

data = [trace]
layout = go.Layout(
    title='Histogram of SAP BW system versions',
    xaxis=dict(type='category'),
)

fig = go.Figure(data=data, layout=layout)
py.iplot(fig, filename='Histogram of BW system versions')

#total and average number of users
#print(df["USER_TOTAL"])
total_nr_users = df["USER_TOTAL"].sum()
average_nr_users = df["USER_TOTAL"].mean()
print("Total number of users (all analyzed systems): " + str(total_nr_users))
print("Average number of users per system: " + str(average_nr_users))

# make a graph of total number of users
trace = go.Bar(
    y=[total_nr_users],
    x=['Total number of users'],
    marker=dict(color=['#ABD9F4']),
    opacity=0.8,
    width = 0.3,
)

data = [trace]
layout = go.Layout(
    title='Total number of users',
)

fig = go.Figure(data=data, layout=layout)
py.iplot(fig, filename='Total number of users')

# make a graph of average number of users
trace = go.Bar(
    y=[average_nr_users],
    x=['Average number of users'],
    marker=dict(color=['#E45751']),
    opacity=0.8,
    width = 0.3,
)

data = [trace]
layout = go.Layout(
    title='Average number of users per system',
)

fig = go.Figure(data=data, layout=layout)
py.iplot(fig, filename='Average number of users')


# Pearson correlation between different features
df_numeric = df.convert_objects(convert_numeric=True)

numeric = ['FREESPACE_GB', 'SIZEUSED_GB', 'WDSO','Cube D', 'Cube E', 'Cube F', 'DSO',
           'AGGR_RATIO', 'AGGR_SIZE', 'APPSERV_NUM',
           'Aggregate', 'Authorization objects', 'BADIs', 'CUBES_SIZE',
           'Changelog', 'Classes and interfaces', 'DUMPS', 'Errorstack', 'FREESPACE_PERC', 'DTP_RESULT',
           'Function groups', 'Function modules', 'HK_DATA_SYS', 'Function groups', 'Function modules',
           'HK_DATA_SYS', 'HK_TOTAL_SIZE', 'Housekeeping', 'INFP_NR_CUBES', 'INFP_NR_DSOS', 'IPPA_AVERAGE', 'IPPA_NUMBER',
           'Lock objects', 'MD_UNUSED_SIZE' , 'Master Data', 'Message classes',
           'NBR_CPU', 'NBR_QUERY', 'OLDB_GROWTH', 'OLDB_PERC', 'Other data', 'PERCENTG', 'PERCENTM', 'PERCENTO',
           'PSA', 'Programs', 'QUERY_RUNTIME', 'QUERY_TOTAL', 'QUERY_UNUSED',
           'RSRV_ERRORS', 'RSRV_WARNINGS', 'SYS_SP', 'Screens', 'TMP_SIZE', 'Transactions',
           'Type groups', 'USER_LOCKED', 'USER_TOTAL', 'USER_UNUSED']

data = [
    go.Heatmap(
    z = df_numeric[numeric].astype(float).corr().values,
    x = df_numeric[numeric].columns.values,
    y = df_numeric[numeric].columns.values,
    colorscale='Viridis',
    reversescale = False,
    text = True,
    opacity = 1.0,
    xgap = 2,
    ygap = 2
    )
]

layout = go.Layout(
    title='Pearson correlation of numerical features',
    xaxis = dict(range=[-1, 55.5], ticks= "", fixedrange = True, tickmode = "auto", nticks = 60, tickfont=dict(size=9)),
    yaxis = dict(range=[-1, 55.5], ticks= "", fixedrange = True, tickmode = "auto", nticks = 60, tickfont=dict(size=9)),
    width = 1000, height = 1000
)

fig = go.Figure(data=data, layout=layout)
py.iplot(fig, filename='labelled-heatmap')

#test of filtering based on industry - energy
df_energy = (df[df.Industry == 'Energy'])

df_energy.describe

new_data_perc = df_energy["PERCENTG"].mean()
print("Hot data requests (<1 year): " + str('%g' % new_data_perc) + "%")

mid_data_perc = df_energy["PERCENTM"].mean()
print("Warm data requests (1-2 years): " + str('%g' % mid_data_perc) + "%")

old_data_perc = df_energy["PERCENTO"].mean()
print("Cold data requests (>2 years): " + str('%g' % old_data_perc) + "%")


# we'll export the df into excel first, to identify what needs to be fixed before jumping in into analysis
print('Exporting final scope to Excel file...')
resultFile = Path("../Data/" + folder_name + "/output.xlsx").resolve()
df.to_excel(str(resultFile))
print('Done exporting final scope to Excel file. Filepath: ' + str(resultFile))
print('All done!')
